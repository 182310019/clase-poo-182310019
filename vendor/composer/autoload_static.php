<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit539d1408b490a931b7f6f01aebe8f6a3
{
    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInit539d1408b490a931b7f6f01aebe8f6a3::$classMap;

        }, null, ClassLoader::class);
    }
}
